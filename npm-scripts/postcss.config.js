module.exports = {
    map: {inline: false},
    plugins: [
        require('postcss-import')(),
        require('postcss-mixins')(),
        require('postcss-nested')(),
        require('postcss-advanced-variables')(),
        require('postcss-preset-env')(),
        require('postcss-normalize')(),
        require('cssnano')({
            autoprefixer: false
        })
    ]
};