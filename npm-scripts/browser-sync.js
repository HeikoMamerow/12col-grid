#!/usr/bin/env node

// require the module as normal
var bs = require("browser-sync").create();

// .init starts the server
bs.init({
  proxy: {
    target: "http://localhost/"
  },
  files: ["build/**/*"],
  notify: false,
  open: false,
  reloadOnRestart: true
});
